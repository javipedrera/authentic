<?php

namespace App\Http\Controllers;

use Symfony\Component\HttpFoundation\JsonResponse;

class ApiController extends Controller
{
    /**
     * @var string
     */
    private $message = '';

    /**
     * @param array $data
     * @param int $statusCode
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondSuccess(array $data = [], int $statusCode = JsonResponse::HTTP_OK)
    {
        return $this->respond($data, $statusCode);
    }

    /**
     * @param array $data
     * @param int $statusCode
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondError(array $data = [], $statusCode = JsonResponse::HTTP_INTERNAL_SERVER_ERROR)
    {
        return $this->respond($data, $statusCode);
    }

    /**
     * @param array $data
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondNotFound(array $data = [])
    {
        return $this->respond($data, JsonResponse::HTTP_NOT_FOUND);
    }

    /**
     * @param array $errors
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithValidationErrors(array $errors)
    {
        if (! $this->message) {
            $this->setMessage('There were validation errors');
        }

        return $this->respond($errors, JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
    }

    /**
     * @param $data
     * @param int $statusCode
     * @return \Illuminate\Http\JsonResponse
     */
    private function respond(array $data = [], $statusCode = 200)
    {
        $response = array_filter([
            'message' => $this->getMessage(),
            'data' => $data,
        ]);
        
        return response()->json($response, $statusCode);
    }

    /**
     * @return mixed
     */
    protected function getMessage()
    {
        return $this->message;
    }

    /**
     * @param string $message
     * @return $this
     */
    protected function setMessage(string $message)
    {
        $this->message = $message;
        
        return $this;
    }
}
