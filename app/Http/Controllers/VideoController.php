<?php

namespace App\Http\Controllers;

use App\Http\Requests\VideoRequest;
use App\Models\Video;
use App\Repositories\RepositoryContract;
use App\Repositories\VideoRepository;
use App\Services\VideoService;
use App\Transformers\VideoTransformer;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class VideoController extends ApiController
{
    /**
     * @var VideoRepository
     */
    private $repository;

    /**
     * @var VideoTransformer
     */
    private $transformer;

    /**
     * @var VideoService
     */
    private $service;

    /**
     * @param RepositoryContract $repository
     * @param VideoTransformer $transformer
     * @param VideoService $service
     */
    public function __construct(RepositoryContract $repository, VideoTransformer $transformer, VideoService $service)
    {
        $this->repository = $repository;
        $this->transformer = $transformer;
        $this->service = $service;
    }
    
    /**
     * @param $username
     * @return \Illuminate\Http\JsonResponse
     */
    public function videosFromUser($username)
    {
        try {
            return $this->respondSuccess([
                'total_videos_size' => $this->repository->getTotalVideoSizeFrom($username),
            ]);
        } catch (ModelNotFoundException $exception) {
            return $this->setMessage('User not found')->respondNotFound();
        } catch (\Exception $exception) {
            return $this->respondError();
        }
    }

    /**
     * @param int $videoId
     * @return \Illuminate\Http\JsonResponse
     */
    public function video(int $videoId)
    {
        try {
            $response = $this->transformer->transform(
                $this->repository->getById($videoId)
            );

            return $this->respondSuccess($response);
        } catch (ModelNotFoundException $exception) {
            return $this->setMessage($exception->getMessage())->respondNotFound();
        }
    }

    /**
     * @param VideoRequest $request
     * @param Video $video
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(VideoRequest $request, Video $video)
    {
        try {
            $errors = $request->getErrorsFromRequest();
            if (! empty($errors)) {
                return $this->respondWithValidationErrors($errors);
            }
            $this->service->update($video, $request->only('size', 'viewers_count'));
            
            return $this->setMessage('Data updated successfully')->respondSuccess();
        } catch (ModelNotFoundException $exception) {
            return $this->setMessage($exception->getMessage())->respondNotFound();
        } catch (\Exception $exception) {
            return $this->setMessage('There were an error while trying to update the resource')
                ->respondError();
        }
    }
}
