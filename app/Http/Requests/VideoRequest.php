<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;

class VideoRequest extends FormRequest
{
    /**
     * @var array
     */
    private $errors = [];
    
    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            'viewers_count' => 'sometimes|integer|min:0|max:4294967295',
            'size' => 'sometimes|integer|min:0|max:16777215',
        ];
    }

    /**
     * @param Validator $validator
     * @return void
     */
    protected function failedValidation(Validator $validator)
    {
        $this->errors = $validator->errors()->messages();
    }

    /**
     * @return array
     */
    public function getErrorsFromRequest()
    {
        return $this->errors;
    }
}
