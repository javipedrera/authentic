<?php

namespace App\Providers;

use App\Http\Controllers\VideoController;
use App\Repositories\RepositoryContract;
use App\Repositories\VideoRepository;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->when(VideoController::class)
            ->needs(RepositoryContract::class)
            ->give(function () {
                return app(VideoRepository::class);
            });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
