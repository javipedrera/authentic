<?php

namespace App\Repositories;

interface RepositoryContract
{
    /**
     * @param int $id
     * @return mixed
     */
    public function getById(int $id);
}
