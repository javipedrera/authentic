<?php

namespace App\Repositories;

use App\Models\User;
use App\Models\Video;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class VideoRepository implements RepositoryContract
{
    /**
     * @param $username
     * @return int
     * @throws \Exception
     */
    public function getTotalVideoSizeFrom($username)
    {
        $user = User::where('name', $username)->firstOrFail()->load('videos');
        return $user->videos->sum('size');
    }

    /**
     * @param $id
     * @throws ModelNotFoundException
     * @return Video
     */
    public function getById(int $id)
    {
        $video = Video::find($id);
        
        if (! $video) {
            throw new ModelNotFoundException('Video not found');
        }
        
        return $video;
    }
}
