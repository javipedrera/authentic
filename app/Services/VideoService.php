<?php

namespace App\Services;

use App\Models\Video;

class VideoService
{
    /**
     * @param Video $video
     * @param $data
     * @return Video
     * @throws \Exception
     */
    public function update(Video $video, $data)
    {
        $video->update($data);
        
        return $video;    
    }
}
