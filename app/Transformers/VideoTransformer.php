<?php

namespace App\Transformers;

use App\Models\User;
use App\Models\Video;

class VideoTransformer
{
    /**
     * @param Video $video
     * @return array
     */
    public function transform(Video $video)
    {
        return [
            'size' => $video->size,
            'viewers_count' => $video->viewers_count,
            'created_by' => array_only($video->user->toArray(), ['id', 'name', 'email']),
        ];
    }

    /**
     * @param User $user
     * @return User
     */
    public function transformFromUser(User $user)
    {
        $user->videos = $user->videos->map(function (Video $video) {
            return [
                'id' => $video->id,
                'size' => $video->size,
                'viewers_count' => $video->viewers_count,
            ];
        });
        
        return $user;
    }
}
