<?php

use App\Models\Video;
use Faker\Generator as Faker;

$factory->define(Video::class, function (Faker $faker) {
    return [
        'size' => $faker->numberBetween(10, 10000000),
        'viewers_count' => $faker->numberBetween(10, 100000000),
    ];
});
