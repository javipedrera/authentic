<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createUsersWithVideos(1, ['name' => 'foo']);
        $this->createUsersWithVideos(30);
    }

    /**
     * @param $amount
     * @param array $userData
     */
    private function createUsersWithVideos($amount, $userData = [])
    {
        factory(\App\Models\User::class, $amount)->create($userData)->each(function ($user) {
            factory(\App\Models\Video::class, 30)->create([
                'user_id' => $user->id,
            ]);
        });
    }
}
