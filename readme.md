# Javier Pedrera - Authentic Co Test

### Set up
In order to run this project properly:

- Clone this repository
- Run `composer install`
- Configure your .env properly by copying the _.env.example_ and tweaking yours
- Run the migrations and seeders with `php artisan migrate --seed`
- Available endpoints:
     - GET: **/api/users/{username}/total-videos-size** (A user called _foo_ has been created to use here)
     - GET: **/api/videos/{videoId}**
     - PATCH: **/api/videos/{video}**

### Decisions and open points
- Controller is using both model binding and repository approach in order to show the usage of both of them.
- API Authentication, policies and middleware as well as some other framework features could be implemented giving proper authorization, but I preferred to spend the time on some other areas rather than the framework's resources.
- More strict validation rules could be implemented


