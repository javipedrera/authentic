<?php

Route::get('users/{username}/total-videos-size', 'VideoController@videosFromUser');

Route::group(['prefix' => 'videos'], function () {
    Route::get('{videoId}', 'VideoController@video');
    Route::patch('{video}', 'VideoController@update');
});
