<?php

namespace Tests\Feature;

use App\Models\User;
use App\Models\Video;
use Faker\Factory;
use Faker\Generator;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Symfony\Component\HttpFoundation\JsonResponse;
use Tests\TestCase;

class ExampleTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * @var User
     */
    private $user;

    /**
     * @var Video
     */
    private $video;

    /**
     * @var Generator
     */
    private $faker;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();

        $user = $this->createUserWithVideos();
        $this->user = $user;
        $this->video = $user->videos->first();
        $this->faker = Factory::create();
    }

    /**
     * @return void
     */
    public function testItRespondWithTotalUserVideoSize()
    {
        $response = $this->get("/api/users/{$this->user->name}/total-videos-size");
        $response->assertExactJson([
            'data' => [
                'total_videos_size' => (int) $this->video->size,
            ]
        ]);
    }

    /**
     * @return void
     */
    public function testItRespondWithVideoData()
    {
        $response = $this->get("/api/videos/{$this->video->id}");

        $response->assertExactJson([
            'data' => [
                'size' => $this->video->size,
                'viewers_count' => $this->video->viewers_count,
                'created_by' => [
                    'id' => $this->user->id,
                    'name' => $this->user->name,
                    'email' => $this->user->email,
                ],
            ]
        ]);
    }

    /**
     * @return void
     */
    public function testItUpdatesVideo()
    {
        $data = ['size' => 10000, 'viewers_count' => 10000];
        $this->patch("/api/videos/{$this->video->id}", $data);

        $this->assertDatabaseHas('videos', $data);
    }

    /**
     * @return void
     */
    public function testItRespondsNotFound()
    {
        $response = $this->get('/api/videos/123784238794');
        $response->assertStatus(JsonResponse::HTTP_NOT_FOUND);
        $response->assertExactJson(['message' => 'Video not found']);
    }

    /**
     * @param int $amount
     * @return User
     */
    private function createUserWithVideos($amount = 1)
    {
        /** @var User $user */
        $user = factory(User::class)->create(['name' => 'foo']);
        factory(Video::class, $amount)->create(['user_id' => $user->id]);

        return $user->load('videos');
    }
}
